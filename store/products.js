export const state=()=>({
    products:[],
    productFilter:[]
  })

export const getters={
  getProductsFilter(state){return state.productFilter},
}
export const mutations={
  setProducts: function (state, data) {
    state.products = data
  },
  setProductsFilter: function(state, data) {
    state.productFilter = data
  },
}
  
export const actions={
  async recuperarProducts({commit} ){
    try{
      await this.$axios.$get('/products').then(response => {
        commit('setProducts',response)
        commit('setProductsFilter',response)
      })
    } catch(error){
      console.log(error)
    }
  },
  filterCategory({commit, state},data){
    console.log(data);
    const datafilter = state.products.filter(function(product){
      return product.category==data;
    })
    commit('setProductsFilter',datafilter)
  }
}