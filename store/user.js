export const state=()=>({
    user:''
  })
  
  export const getters={
    getUser(state){return state.user}
  }
  
  export const mutations={
    setUser: function (state, data) {
      state.user = data
    }
  }
  
  export const actions= {
    actualizarUser({commit},dato){
      commit('setUser',dato)
    }
  }