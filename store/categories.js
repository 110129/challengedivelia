export const state=()=>({
    categories:[],
  })

export const getters={
  getCategories(state){return state.categories},
}
export const mutations={
  setCategories: function (state, data) {
      state.categories = data
  },
}
  
export const actions={
  async recuperarCategories({commit} ){
      try{
        await this.$axios.$get('/products/categories').then(response => {
            commit('setCategories',response)
        })
      } catch(error){
        console.log(error)
      }
  },
}